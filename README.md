# cours-vue

## Extensions

vue dev-tools

## Dependencies
    - nodejs
    - npm

## Project setup
```
npm install
npm install -g @vue/cli

# you shouldn't have Uppercase in your folder name
vue create .
```

## Project commands

**Run project in dev**
```
npm run serve
```

**Compiles for production**
```
npm run build
```

**Lints and fixes files**
```
npm run lint
```


## File architecture
   **/components**
        - contains every .vue files
        
   **.vue** 
        - One component with html/css/js
        
## Create a component
**export**
```javascript
    export default {
        name: 'componentName',
        components : {
            componentChild1,
            componentChild2
        },
        data: () =>({
            theme: "dark"
        }),
        methods : {
            toggleTheme : function (theme){
                this.$data.theme = theme == "dark" ? "light" : "dark";
            }
        }
    }
```

**style**

Scoped style will be applied only on current component

`<style scoped>`

**template**
```html
<template>
  <div id="app">
    <h1>{{Theme}}</h1>
    <img alt="Vue logo" src="./assets/logo.png">
    <componentChild1 msg="Welcome to Your Vue.js App"/>
    <componentChild2 msg="Welcome to Your Vue.js App"/>
  </div>
</template>
```


## Data types

props: data send from composants to other composants

data : 

    -create dynamisme
    -we cannot return an object directly, we have to use a function that return an object
    -computed property : wont change on every rander    



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
